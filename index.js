import express from 'express';
import fetch from 'node-fetch';

const app  = express();
const port = process.env.port || 3000;

app.get('/' ,(req , res)=>{
    res.json({"message":"home api"});
});

app.get('/provinsi' , (req , res)=>{
    return fetch('http://dev.farizdotid.com/api/daerahindonesia/provinsi' , {
        method:'get'
    }).then(res => res.json())
      .then(data =>{
        res .json(data.semuaprovinsi)d
      })
});

app.get('/ping' , (req , res)=>{
    return fetch('https://api.vasdev.co.id:8066/comm/',{
        method:'post',
        body:{"type":"ping"}
    }).then(res => res.json())
      .then(data =>{
        res.json(data)
      })
});

app.get('/perkali/:angka1/:angka2' , (req , res) =>{
    let { angka1 , angka2 } = req.params;

    res.json(parseFloat(angka1) + parseFloat(angka2))
})


app.listen(port , ()=>{
    console.log(`Server berjalan di port ${port}`)
});